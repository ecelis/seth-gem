Gem::Specification.new do |s|
  s.name        = 'setl_tool'
  s.version     = '1.0.1'
  s.date        = '2014-08-05'
  s.summary     = "Simple ETL"
  s.description = "Just what the summary and name implies"
  s.authors     = ["Ernesto Celis"]
  s.email       = 'ecelis@sdf.org'
  s.files       = ["lib/setl_tool.rb", "lib/setl_tool/extract.rb", "lib/setl_tool/load.rb", "lib/setl_tool/transform.rb"]
  s.homepage    =
    'https://bitbucket.org/ecelis/seth-gem'
  s.license       = 'Apache 2.0'
end
