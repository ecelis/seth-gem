# Licensed to you under one or more contributor license agreements.
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.  Ernesto Angel Celis de la
# Fuente licenses this file to you under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with the
# License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'setl_tool/extract'
require 'setl_tool/transform'
require 'setl_tool/load'

class Setl_tool

  attr_accessor :target_db
  attr_accessor :target_table
  attr_reader :extract
  attr_reader :transform
  attr_accessor :load

  # TODO Change headers to true
  def initialize(src_type = "csv",
                 headers = false,
                src_path)
    @src_type = src_type
    @headers = headers
    @src_path = src_path
    @extract = Extract.new(@src_type,
                           @headers,
                           @src_path)
    @transform = Transform.new
    #@load = Load.new
  end

end
