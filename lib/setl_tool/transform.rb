# Licensed to you under one or more contributor license agreements.
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.  Ernesto Angel Celis de la
# Fuente licenses this file to you under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with the
# License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Transforms from an in memory table to a known SQL dialect statement
class Transform

  # Target Table in RDMS engine
  attr_accessor :target_table
  # In-Memory source data table
  attr_accessor :table
  # don't use, reserved
  attr_reader :values
  # Valid SQL dialect statement
  attr_reader :stmt

  # Currently only CSV to PostgreSQL is supported
  def initialize(src_type = "csv",
                target_db = "pg")
    @src_type = src_type
    @target_db = target_db
    if @target_table == nil
      @target_table = "#{@src_type}_import"
    end
    @values = Array.new
  end

  # Execute data transformation
  def data
    case @src_type
      when "csv" then process_csv
    end
    case @target_db
    when "pg" then to_pg
    end
  end

  private
  def process_csv
    @table.each do |row|
      values = row.map(&:inspect).join(', ')
      stmt = "#{values}"
      @values << stmt.gsub('"', "'")
    end
  end

  private
  def to_pg
    values = ""
    @values.each do |row|
      values << "("
      values << row
      values << "),"
    end
    @stmt = "INSERT INTO #{@target_table} VALUES #{values.chop!};"
  end
end


