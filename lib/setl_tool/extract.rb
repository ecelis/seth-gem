# Licensed to you under one or more contributor license agreements.
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.  Ernesto Angel Celis de la
# Fuente licenses this file to you under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with the
# License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require "csv"

# Extract data into a in-memory table
class Extract

  # In-memory table
  attr_reader :table

  # Currently src_type only 'csv' is supported
  # Headers true if source has column names in first row
  # src_path is an absolute path to the csv file
  def initialize(src_type,
                 headers,
                 src_path)
    @src_type = src_type
    @headers = headers
    @src_path = src_path
  end

  # Extract data from @src_type
  def data
    case @src_type
    when "csv"
      @table = CSV.read(@src_path,
                        :headers => @headers)
      # TODO FIX headers
      #@col_headers = @table.headers
      #if @headers
      #  @table.by_col!
      #@table.delete(0)
      #end
    else
      puts "xlsl"
    end
  end
end
