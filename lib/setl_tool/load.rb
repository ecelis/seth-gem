# Licensed to you under one or more contributor license agreements.
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.  Ernesto Angel Celis de la
# Fuente licenses this file to you under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with the
# License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'pg'

class Load
  attr_accessor :db_host
  attr_accessor :db_name
  attr_accessor :db_user
  attr_accessor :db_passwd
  attr_accessor :conn

  def initialize(db_host, db_name, db_user, db_passwd)
    @conn = PG.connect(host: db_host,
                       dbname: db_name,
                       user: db_user,
                       password: db_passwd)
    @conn.set_error_verbosity(PG::PQERRORS_VERBOSE)
  end

  def insert(stmt)
    @conn.exec(stmt)
  end
end
