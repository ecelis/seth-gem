Simple ETL Tool
===============

[![Gem Version](https://badge.fury.io/rb/setl_tool.svg)](http://badge.fury.io/rb/setl_tool)

Simple Extract, Load and Transform Tool

Features
--------

 * CSV to PostgreSQL


TODO
----

 * DROP TABLE IF EXISTS before table creation
 * Map source columns to target columns
 * xlsx import support

Install
-------

    gem install setl_tool

From source

    git clone https://ecelis@bitbucket.org/ecelis/seth-gem.git


Usage
-----

    irb -Ilib -rsetl_tool
    etl = Setl_tool.new './data/source.csv'
    etl.extract.data
    etl.transform.table = etl.extract.table
    etl.transform.data
    etl.load = Load.new 'localhost', 'dbname', 'dbuser', 'dbpasswd'
    etl.load.insert(etl.transform.stmt)

Known Bugs
----------

 * Target table should exists before trying to load anything

License
-------

    Licensed to you under one or more contributor license agreements.
    See the NOTICE file distributed with this work for additional
    information regarding copyright ownership.  Ernesto Angel Celis de la
    Fuente licenses this file to you under the Apache License, Version 2.0
    (the "License"); you may not use this file except in compliance with the
    License.  You may obtain a copy of the License at
    
      http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
